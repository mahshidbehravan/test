import { async, TestBed, fakeAsync, ComponentFixture, tick } from '@angular/core/testing';
import { AppModule } from './app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { SpyLocation } from '@angular/common/testing';
import { Router } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { Location } from '@angular/common';
import { ClassComponent } from './home/class/class.component';
import { HomeModule } from './home/home.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers } from '.';


describe('App router & RouterTestingModule', () => {

    let fixture: ComponentFixture<AppComponent>;
    let router: Router;
    let location: SpyLocation;

    beforeEach(async(() => {
        TestBed.configureTestingModule({

            declarations: [AppComponent],
            imports: [HomeModule,
                RouterTestingModule,
                StoreModule.forRoot(reducers),
                EffectsModule.forRoot([])]
        });

        router = TestBed.get(Router);
        location = TestBed.get(Location);

        fixture = TestBed.createComponent(AppComponent);
        router.initialNavigation();
    }));
    it('navigate to "" redirects you to /home', fakeAsync(() => {
        router.navigate(['']);
        tick();
        expect(location.path()).toBe('/');
    }));
});
