import { TestBed, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { EditNewComponent } from './edit-new.component';
import { ShareModule } from 'src/app/share/share.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement, Component, NgModule } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';



describe('Component: EditNewComponent', () => {

    let component: EditNewComponent;
    let fixture: ComponentFixture<EditNewComponent>;
    let de: DebugElement;
    let dialog: MatDialog;
  
    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [EditNewComponent],
            imports: [
                ShareModule,
                ReactiveFormsModule,
                BrowserAnimationsModule],
            providers: [
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: {} },
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });

        fixture = TestBed.createComponent(EditNewComponent);
        component = fixture.componentInstance;
        dialog = TestBed.get(MatDialog);

    });
});

