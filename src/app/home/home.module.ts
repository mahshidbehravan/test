import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { ShareModule } from '../share/share.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClassComponent } from './class/class.component';
import { ClassMockService } from '../services/class.service.mock';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { HomeEffects } from './store/home.effect';
import { reducers } from './store/index';
import { EditNewComponent } from './class/edit-new/edit-new.component';
import { StudentComponent } from './student/student.component';
import { StudentEditNewComponent } from './student/edit-new/edit-new.component';
import { StudentMockService } from '../services/student.mock';

@NgModule({
  declarations: [HomeComponent, ClassComponent, EditNewComponent, StudentComponent , StudentEditNewComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ShareModule,

    EffectsModule.forFeature([HomeEffects]),
    StoreModule.forFeature('home', reducers),

  ],
  entryComponents: [EditNewComponent , StudentEditNewComponent],
  providers: [ClassMockService , StudentMockService]
})
export class HomeModule { }
