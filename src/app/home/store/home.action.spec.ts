import { GetClassAction, ActionTypes, GetClassListAction, EditClassAction,
     AddClassAction, DeleteClassAction, SelectedClassAction,
      GetStudentAction, GetStudentListAction, AddStudentAction, EditStudentAction, DeleteStudentAction } from './home.action';
import { ClassModel } from 'src/app/models/class';
import { StudentModel } from 'src/app/models/student';

describe('Actions', () => {
    it('should create an GetClassAction', () => {
        const action = new GetClassAction();
        expect(action.type).toEqual(ActionTypes.Get_Class);

    });

    it('should create an GetClassListAction', () => {
        const payload: ClassModel[] = [
            { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' },
            { id: 2, className: 'English', location: 'Building 3 Room 134', teacher: 'Miss Sanderson' }
        ];
        const action = new GetClassListAction(payload);

        expect({ ...action }).toEqual({
            type: ActionTypes.Get_Class_List,
            payload,
        });
    });
    it('should create an EditClassAction', () => {
        const payload: ClassModel = { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' };
        const action = new EditClassAction(payload);

        expect({ ...action }).toEqual({
            type: ActionTypes.Edit_Class,
            payload,
        });
    });
    it('should create an AddClassAction', () => {
        const payload: ClassModel = { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' };

        const action = new AddClassAction(payload);

        expect({ ...action }).toEqual({
            type: ActionTypes.Add_Class,
            payload,
        });
    });
    it('should create an DeleteClassAction', () => {
        const payload: ClassModel = { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' };

        const action = new DeleteClassAction(payload);

        expect({ ...action }).toEqual({
            type: ActionTypes.Delete_Class,
            payload,
        });
    });
    it('should create an SelectedClassAction', () => {
        const payload: ClassModel = { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' };

        const action = new SelectedClassAction(payload);
        expect(action.type).toEqual(ActionTypes.Selected_Class);

    });
    it('should create an GetStudentAction', () => {
        const action = new GetStudentAction(1);
        expect(action.type).toEqual(ActionTypes.Get_Student);

    });
    it('should create an GetStudentListAction', () => {
        const payload: StudentModel[] = [
            { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 },
            { id: 2, studentName: 'Peter Parker', age: 19, gpa: 2.9, classId: 1 },
            { id: 3, studentName: 'Robert Smith', age: 18, gpa: 3.1, classId: 1 },
            { id: 4, studentName: 'Rebecca Black', age: 19, gpa: 2.1, classId: 1 },
            { id: 5, studentName: 'Mahshid Behravan', age: 18, gpa: 10, classId: 2 },
            { id: 6, studentName: 'Mohammad Soofi', age: 19, gpa: 10, classId: 2 },
        ];
        const action = new GetStudentListAction(payload);
        expect(action.type).toEqual(ActionTypes.Get_Student_List);

    });

it('should create an AddStudentAction', () => {
    const payload: StudentModel = { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 };

    const action = new AddStudentAction(payload);
    expect(action.type).toEqual(ActionTypes.Add_Student);

});
it('should create an EditStudentAction', () => {
    const payload: StudentModel = { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 };

    const action = new EditStudentAction(payload);
    expect(action.type).toEqual(ActionTypes.Edit_Student);

});
it('should create an DeleteStudentAction', () => {
    const payload: StudentModel = { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 };

    const action = new DeleteStudentAction(payload);
    expect(action.type).toEqual(ActionTypes.Delete_Student);

});

});




