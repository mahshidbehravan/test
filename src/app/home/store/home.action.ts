
import { Action } from '@ngrx/store';
import { ClassModel } from '../../models/class';
import { StudentModel } from 'src/app/models/student';

export const ActionTypes = {

    Get_Class: '[Class] Get_Class',
    Get_Class_List: '[Class] Get_Class_List',
    Edit_Class: '[Class] Edit_Class',
    Add_Class: '[Class] Add_Class',
    Delete_Class: '[Class] Delete_Class',
    Selected_Class: '[Class] Selected_Class',

    Get_Student: '[Student] Get_Student',
    Get_Student_List: '[Student] Get_Student_List',
    Add_Student: '[Student] Add_Student',
    Edit_Student: '[Student] Edit_Student',
    Delete_Student: '[Student] Delete_Student',




};

export class GetClassAction implements Action {
    type = ActionTypes.Get_Class;
    constructor(public payload: any = null) { }
}
export class GetClassListAction implements Action {
    type = ActionTypes.Get_Class_List;
    constructor(public payload: ClassModel[]) { }
}
export class EditClassAction implements Action {
    type = ActionTypes.Edit_Class;
    constructor(public payload: ClassModel) { }
}
export class AddClassAction implements Action {
    type = ActionTypes.Add_Class;
    constructor(public payload: ClassModel) { }
}
export class DeleteClassAction implements Action {
    type = ActionTypes.Delete_Class;
    constructor(public payload: ClassModel) { }
}

export class SelectedClassAction implements Action {
    type = ActionTypes.Selected_Class;
    constructor(public payload: ClassModel) { }
}
export class GetStudentAction implements Action {
    type = ActionTypes.Get_Student;
    constructor(public payload: number) { }
}
export class GetStudentListAction implements Action {
    type = ActionTypes.Get_Student_List;
    constructor(public payload: StudentModel[]) { }
}

export class AddStudentAction implements Action {
    type = ActionTypes.Add_Student;
    constructor(public payload: StudentModel) { }
}
export class EditStudentAction implements Action {
    type = ActionTypes.Edit_Student;
    constructor(public payload: StudentModel) { }
}
export class DeleteStudentAction implements Action {
    type = ActionTypes.Delete_Student;
    constructor(public payload: StudentModel) { }
}
export type Actions
    = GetClassAction
    | GetClassListAction
    | EditClassAction
    | AddClassAction
    | DeleteClassAction
    | SelectedClassAction
    | GetStudentAction
    | GetStudentListAction
    | AddStudentAction
    | EditStudentAction
    | DeleteStudentAction
;




