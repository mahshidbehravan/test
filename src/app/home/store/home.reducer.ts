import * as actions from './home.action';
import { ClassModel } from '../../models/class';
import { StudentModel } from 'src/app/models/student';


export interface State {

    ClassData: any;
    ClassList: ClassModel[];
    EditClass: ClassModel;
    AddClass: ClassModel;
    DeleteClass: ClassModel;
    SelectedClass: ClassModel;
    StudentData: any;
    StudentList: StudentModel[];
    AddStudent: StudentModel;
    EditStudent: StudentModel;
    DeleteStudent: StudentModel;



}
export const INITIAL_STATE: State = {
    ClassData: null,
    ClassList: [],
    EditClass: null,
    AddClass: null,
    DeleteClass: null,
    SelectedClass: null,
    StudentData: null,
    StudentList: [],
    AddStudent: null,
    EditStudent: null,
    DeleteStudent: null


};

export function reducer(state: State = INITIAL_STATE, action: actions.Actions): State {

    switch (action.type) {
        case actions.ActionTypes.Get_Class:
            {
                return Object.assign({}, state, {
                    ClassData: action.payload
                });
            }
            case actions.ActionTypes.Get_Class_List:
            {
                return Object.assign({}, state, {
                    ClassList: action.payload
                });
            }
            case actions.ActionTypes.Edit_Class:
            {
                return Object.assign({}, state, {
                    EditClass: action.payload
                });
            }
            case actions.ActionTypes.Add_Class:
            {
                return Object.assign({}, state, {
                    AddClass: action.payload
                });
            }

            case actions.ActionTypes.Delete_Class:
            {
                return Object.assign({}, state, {
                    DeleteClass: action.payload
                });
            }
            case actions.ActionTypes.Selected_Class:
            {
                return Object.assign({}, state, {
                    SelectedClass: action.payload
                });
            }
            case actions.ActionTypes.Get_Student:
            {
                return Object.assign({}, state, {
                    StudentData: action.payload
                });
            }
            case actions.ActionTypes.Get_Student_List:
            {
                return Object.assign({}, state, {
                    StudentList: action.payload
                });
            }
            case actions.ActionTypes.Add_Student:
            {
                return Object.assign({}, state, {
                    AddStudent: action.payload
                });
            }
            case actions.ActionTypes.Edit_Student:
            {
                return Object.assign({}, state, {
                    EditStudent: action.payload
                });
            }
            case actions.ActionTypes.Delete_Student:
            {
                return Object.assign({}, state, {
                    DeleteStudent: action.payload
                });
            }
        default: {
            return state;
        }
    }
}
export const getClassList = (state: State) => state.ClassList;
export const getStudentList = (state: State) => state.StudentList;
export const getSelectedClass = (state: State) => state.SelectedClass;











