import {
    ActionReducer, combineReducers, ActionReducerMap, createFeatureSelector, createSelector
  } from '@ngrx/store';
  import { compose } from '@ngrx/core/compose';
  import * as fromHome from './home.reducer';
  import * as fromReducer from './home.reducer';
  import * as fromRoot from '../../index';

export interface HomeState {
  HomeState: fromReducer.State;
  }
  export interface State extends fromRoot.State {
    Home: HomeState;
  }
  export const reducers: ActionReducerMap<HomeState> = {
    HomeState: fromReducer.reducer,
  };

  const HomeFeatureState = createFeatureSelector<HomeState>('home');
  export const homeState = createSelector(HomeFeatureState, s => s.HomeState);
  export const getclass = createSelector(homeState, fromHome.getClassList);
  export const getstudent = createSelector(homeState, fromHome.getStudentList);
  export const getSelectedClass = createSelector(homeState, fromHome.getSelectedClass);








  



