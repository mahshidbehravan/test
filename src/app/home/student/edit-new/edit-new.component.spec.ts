import { StudentEditNewComponent } from './edit-new.component';
import { ComponentFixture, TestBed, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule, NgForm, ValidatorFn, AbstractControl } from '@angular/forms';
import { ShareModule } from 'src/app/share/share.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { StudentValidationService } from 'src/app/share/uniq-student';
import { StudentMockService } from 'src/app/services/student.mock';

describe('Component: StudentEditNewComponent', () => {

    let component: StudentEditNewComponent;
    let fixture: ComponentFixture<StudentEditNewComponent>;


    class MockStudentService {
        student = [
            { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 },
            { id: 2, studentName: 'Peter Parker', age: 19, gpa: 2.9, classId: 1 },
            { id: 3, studentName: 'Robert Smith', age: 18, gpa: 3.1, classId: 1 },
            { id: 4, studentName: 'Rebecca Black', age: 19, gpa: 2.1, classId: 1 }

        ];
        validStudentMock(value, classId) {
            return { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 };
        }
    }
   // let OneStudent= { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 };



  let form: NgForm;
  
//   class StudentValidation {
//      checkName(mockService: MockStudentService, classId: number, IsNew: boolean) {
//           if (mockService.validStudentMock(OneStudent, 1) && IsNew) {
//             return { 'isExs': false };

//       }
//     }
// }
    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [ReactiveFormsModule, FormsModule, ShareModule],
            declarations: [StudentEditNewComponent],
            providers: [,
               // { provide: StudentValidationService, useValue: StudentValidation },
                { provide: StudentMockService, useClass: MockStudentService },
                { provide: StudentMockService, useClass: MockStudentService },
                { provide: MAT_DIALOG_DATA, useValue: {} },
                { provide: MatDialogRef, useValue: {} },
                { provide: ComponentFixtureAutoDetect, useValue: true },
                NgForm
            ]
        });

        fixture = TestBed.createComponent(StudentEditNewComponent);

        component = fixture.componentInstance;

        component.ngOnInit();
    });
    // it('form invalid when empty', () => {
    //     expect(component.studentForm.valid).toBeFalsy();
    // });

    // it('age ', () => {
    //     let errors = {};
    //     let age = component.studentForm.controls['age'];
    //     expect(age.valid).toBeFalsy();

    //     errors = age.errors || {};
    //     expect(errors['required']).toBeTruthy();
    //     age.setValue(1);
    //     errors = age.errors || {};
    //     expect(errors['required']).toBeFalsy();
    // });
});

