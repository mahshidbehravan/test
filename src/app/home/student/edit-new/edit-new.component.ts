import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { StudentValidationService } from 'src/app/share/uniq-student';
import { StudentMockService } from 'src/app/services/student.mock';

@Component({
  selector: 'app-edit-new',
  templateUrl: './edit-new.component.html',
  styleUrls: ['./edit-new.component.css']
})
export class StudentEditNewComponent implements OnInit {

  public studentForm: FormGroup;
  public IsNew: boolean;
  private id: number;
  private classId:number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  private fb: FormBuilder,
  public thisDialogRef: MatDialogRef<StudentEditNewComponent>,
  private ValidationService: StudentValidationService,
  private MockService: StudentMockService) { }

  ngOnInit() {
    this.studentForm = this.fb.group({
      studentName: ['',  [Validators.required, this.ValidationService.checkName(this.MockService, this.data.classId , this.data.isNew) ]],
      age: ['', Validators.required],
      gpa: ['', Validators.required],
    });

    this.IsNew = this.data.isNew;
    this.classId = this.data.classId;
    if (!this.data.isNew ) {
      this.studentForm.controls.studentName.setValue(this.data.selectedRow.studentName);
      this.studentForm.controls.age.setValue(  this.data.selectedRow.age );
      this.studentForm.controls.gpa.setValue(this.data.selectedRow.gpa);
      this.id = this.data.selectedRow.id;
    } else {
      this.id = this.data.nextId;
    }

  }
  save() {
    const returnValue  = {
      id: this.id,
      studentName: this.studentForm.controls.studentName.value,
      age: this.studentForm.controls.age.value,
      gpa: this.studentForm.controls.gpa.value,
      classId: this.classId
    };
    this.thisDialogRef.close(returnValue);
  }
  close(){
    this.thisDialogRef.close();

  }
}
