import { TestBed, async, inject, ComponentFixture } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import { StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers } from '../store/index';
import { ShareModule } from 'src/app/share/share.module';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentComponent } from './student.component';
import { StudentMockService } from 'src/app/services/student.mock';
import { of } from 'rxjs';
import { MockActivatedRoute } from './mock-active-route';
import { GetStudentAction } from '../store/home.action';
import { hot } from 'jasmine-marbles';

class MockStudentService {
    students = [
        { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 },
        { id: 2, studentName: 'Peter Parker', age: 19, gpa: 2.9, classId: 1 },
        { id: 3, studentName: 'Robert Smith', age: 18, gpa: 3.1, classId: 1 },
        { id: 4, studentName: 'Rebecca Black', age: 19, gpa: 2.1, classId: 1 }
      ];
}
let activeRoute: MockActivatedRoute;
let fixture: ComponentFixture<StudentComponent>;
let component: StudentComponent;

describe('StudentComponent', () => {
beforeEach(() => {
  activeRoute = new MockActivatedRoute();
});
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StudentComponent],

      imports:[ StoreModule.forRoot(reducers),
        RouterTestingModule,
        ShareModule,
        EffectsModule.forRoot([])],
       providers: [StudentComponent , { provide: StudentMockService, useClass: MockStudentService },
        { provide: ActivatedRoute, useValue: activeRoute},
        // {
        //   provide: Store,
        //   useValue: {
        //     dispatch: jest.fn(),
        //     pipe: jest.fn()
        //   }
        // }
      ]
      });

  }));


  it('should create the student', async(() => {
    fixture = TestBed.createComponent(StudentComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
// describe('ngOnInit()', () => {
//   it('should dispatch an the GetStudentAction  in ngOnInit lifecycle', () => {
//     const action = new GetStudentAction(1);
//     const store = TestBed.get(Store);
//     const spy = jest.spyOn(store, 'dispatch');

//     fixture.detectChanges();

//     expect(spy).toHaveBeenCalledWith(action);
//   });

// });
