export class StudentModel {
    id: number;
    studentName: string;
    age: number;
    gpa: number;
    classId: number;
}
