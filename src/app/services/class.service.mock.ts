import { Injectable } from '@angular/core';
import { Observable,  of } from 'rxjs';
import { ClassModel } from '../models/class';

@Injectable()
export class ClassMockService {

  private classes: ClassModel[] = [];
  private nextId: number;

  constructor() {
    const loalStorageItem = JSON.parse(localStorage.getItem('classes'));
    this.classes = [
      { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' },
      { id: 2, className: 'English', location: 'Building 3 Room 134', teacher: 'Miss Sanderson' },
    ];
  }

  public get(): Observable<ClassModel[]> {
    localStorage.setItem('classes', JSON.stringify(this.classes));
    return of(this.classes);

  }

  public edit(classModel: ClassModel): Observable<ClassModel[]> {

    const updateItem = this.classes.find(this.findIndexToUpdate, classModel.id);
    const index = this.classes.indexOf(updateItem);
    this.classes[index] = classModel;

    localStorage.removeItem('classes');
    localStorage.setItem('classes', JSON.stringify(this.classes));
    return of(this.classes);
  }

  public add(classModel: ClassModel): Observable<ClassModel[]> {
    this.classes.push(classModel);
    localStorage.setItem('classes', JSON.stringify(this.classes));
    return of(this.classes);
  }

  public delete(classModel: ClassModel): Observable<ClassModel[]> {
    const index: number = this.classes.indexOf(classModel);
    if (index !== -1) {
        this.classes.splice(index, 1);
    }
    localStorage.removeItem('classes');
    localStorage.setItem('classes', JSON.stringify(this.classes));
    return of(this.classes);
  }

  findIndexToUpdate(newItem) {
    return newItem.id === this;
  }


}
